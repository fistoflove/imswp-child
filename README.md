# IMSWP Child Theme

This is a WordPress child theme meant to be used with the [IMSWP parent theme](https://gitlab.com/imsmarketingie/imswp).

# Getting Started

 1. Install the [IMSWP parent theme](https://gitlab.com/imsmarketingie/imswp).
	- Clone via Git or [Download](https://gitlab.com/imsmarketingie/imswp/-/raw/master/theme.zip?inline=false).
 2. Install a fork of the IMSWP child theme (this)
	- Clone via Git or [Download](https://gitlab.com/fistoflove/imswp-child/-/raw/main/theme.zip?inline=false).
 3. Install the latest version of [ACF](https://www.advancedcustomfields.com/).
 4. CD into the child theme directory and run the following commands:
	 - `npm install`
	 - `sudo npm install -g grunt-cli`
	 - `sudo npm install -g sass`
	 - `grunt watch`
 5. Start coding

## File Structure - Base Theme
Required folders:
 - templates - HTML theme templates.
 - static - Compiled CSS theme base files.
 - dev/scss - SCSS theme base files.
 - blocks - Theme blocks and their assets.
 - assets/fonts - Theme fonts.
 - assets/images - Theme images.
 
## File Structure - Blocks
Required folders:
 - block.json (*required*) - This file should follow the standards set by [WordPress](https://developer.wordpress.org/block-editor/reference-guides/block-api/block-metadata/) and [ACF](https://www.advancedcustomfields.com/resources/acf-blocks-with-block-json/).
 - style.scss (*required*) - This file should contain all block styles.
 - render.php (*required*)
	 - If using PHP templates, this file should contain your block logic and structure (HTML).
	 - If using Timber templates, this file should be used to render the template.twig file.
 - template.twig (*optional*) - If using Timber templates, this file should contain your block strucure (HTML).

# Styles

IMSWP uses [Grunt](https://gruntjs.com/) & [scssphp](https://scssphp.github.io/scssphp/) to compile all theme scss files.

## Base Styles
The base style files can be found in the "dev/scss" folder. SCSS files found in "dev/scss" are compiled into two files:
 - **base.min.css** - This file contains all theme global styles.
	 - This file is included in the theme's `<head></head>` tag.
 - **editor.css** - This file contains all theme global styles.
	 - This file provides block styles to the Gutenberg editor.


## Block Styles
All blocks should contain a style.scss file.

 - **style.min.css**
	  - This file is compiled via the "grunt sass:blocks && grunt csso:blocks" command.
 - **compiled.css**
	 - This file is compiled during block registration via the PHP SCSS compiler.`

Block styles loading order:
 1. **style.min.css** - If this file is present it will be loaded and compiled.css will be ignored.
 2. **compiled.css** - If "style.min.css" is not present, the style.scss file will be compiled into "compiled.css" and loaded.


# Requirements

 - **PHP 8.0**
