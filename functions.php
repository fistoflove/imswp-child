<?php
/**
 * Child Starter functions and definitions
 *
 */

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style('theme-override', get_stylesheet_directory_uri() . '/static/base.min.css', array(), '1.0.0', 'all');
});

?>