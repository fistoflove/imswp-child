<?php

?>

<section class="<?= blockClasses($block) ?>" <?= blockSection($block) ?>>
    <div class="container">
        <div class="flex">
            <InnerBlocks/>
        </div>
    </div>
</section>